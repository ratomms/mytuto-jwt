<div align="center">
	<h1> REST Api avec Slim PHP modifié</h1>
</div>

Ce projet s'inspire du projet de Manuel Gil disponible <a href="https://github.com/ManuelGil/REST-Api-with-Slim-PHP">ici</a>

<a name="started"></a>
## :traffic_light: Getting Started

Pour test ce project dans votre machine local.

<a name="requirements"></a>
### Requirements

  * PHP 5.6
  * MySQL or MariaDB
  * Apache Server(avec Module de Rewrite activé)

<a name="installation"></a>
### Installation

#### Créer une base de données

Executer ce script

```sql
-- -----------------------------------------------------
-- Schema NETWORK
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `NETWORK2` DEFAULT CHARACTER SET utf8 ;
USE `NETWORK2` ;

-- -----------------------------------------------------
-- Table `NETWORK2`.`USERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NETWORK2`.`USERS` (
  `ID_USER` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `USERNAME` VARCHAR(20) NOT NULL,
  `PASSWORD` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID_USER`),
  UNIQUE INDEX `ID_USER_UNIQUE` (`ID_USER` ASC),
  UNIQUE INDEX `USER_UNIQUE` (`USERNAME` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NETWORK`.`QUOTES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NETWORK2`.`QUOTES` (
  `ID_QUOTE` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `QUOTE` VARCHAR(120) NOT NULL,
  `POST_DATE` DATETIME NOT NULL DEFAULT NOW(),
  `LIKES` INT UNSIGNED NOT NULL DEFAULT 0,
  `ID_USER` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`ID_QUOTE`),
  UNIQUE INDEX `ID_QUOTE_UNIQUE` (`ID_QUOTE` ASC),
  INDEX `fk_QUOTES_USERS_idx` (`ID_USER` ASC),
  CONSTRAINT `fk_QUOTES_USERS`
    FOREIGN KEY (`ID_USER`)
    REFERENCES `NETWORK2`.`USERS` (`ID_USER`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
```

#### Copier ce project

  1. Créer un projet dans le répertoire racine de votre serveur Web
     ```bash
     $ mkdir rest
     ```
     
  2. Clone ou Télécharger ce repository
  3. Décompresser l'archive 
  4. Copier tous les fichiers et sous dossier dans le répertoire, à l'occurence "rest", de votre serveur Web
  
#### Tester le projet

  1. Installer le plugin Postman dans Chrome
  2. Faire une requete GET vers <a href="http://localhost/project/rest/api/list">http://localhost/project/mytuto-jwt/api/list</a> et y mettre un header "Authorization" avec la valeur ci-dessous:
  
     * Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWFkZXIiOnsiaWQiOiIyIiwidXNlciI6InRvbW15QG1haWwuY29tIn0sInBheWxvYWQiOnsiaWF0IjoiMjAxOC0wMi0xOCAwOTo1NTo0MSIsImV4cCI6IjIwMTgtMDItMTggMTE6NTU6NDEifX0.vitxGiKabVuxLOpq0MsaJTzoEvEObpmvGHFtw2uNUs8
     
#### Customiser le projet

  1. Pour faire votre propre custom, il suffit d'ouvrir le projet avec votre Editeur favorie (Atom, Sublime, Visual Studio Code, Vim, Brackets, etc) et faire les modifications que vous voulez faire
